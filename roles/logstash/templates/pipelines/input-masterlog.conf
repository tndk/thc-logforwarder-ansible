input {
	file {
		path => "{{ logstash_masterlog_path }}/*.log"
		mode => "read"
		file_completed_action => "delete"
		codec => multiline {
			pattern => "^\+\+\+ "
			negate => "true"
			what => "previous"
		}
	}
}

filter {
	# We use separate mutate blocks, as mutate
	# has specific processing order (https://www.elastic.co/guide/en/logstash/current/plugins-filters-mutate.html)
	mutate {
		copy => { "message" => "original" }
	}
	mutate {
		gsub => ["message", "\n|\r", "\t"]
	}
	grok {
		match => { "message" => "(\+\+\+|\*\*\*) (?<timestamp>.*? .*?) %{GREEDYDATA:message}"}
		overwrite => ["message"]
	}
	date {
		match => [ "timestamp", "yyyy/MM/dd HH:mm:ss.SSS" ]
		remove_field => [ "timestamp" ]
	}
	grok {
		match => {"path" => ".*/(?<host>.*?)_"}
		overwrite => ["host"]
	}
	mutate {
		add_tag => [ "masterlog" ]
		remove_field => [ "path" ]
		add_field => { "datacenter" => "{{ thc_datacenter }}" }
		add_field => { "class" => "{{ thc_class }}" }
	}
}

output {
	pipeline {
		send_to => rabbitmq
	}
}
