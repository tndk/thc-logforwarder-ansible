#!/usr/bin/bash
TIMEOUT=${TIMEOUT:-"10s"}
INTERFACE=${INTERFACE:-"eth1"}
OUTFILE=${1:-"/root/sourcedump.log"}
LOGFILE="/tmp/sourcedump.log"

# Captures packets sent to syslog ports. Pipes into awk one-liner that extracts unique destination IPs.
echo "Capturing unique syslog connections to $INTERFACE for $TIMEOUT into $OUTFILE ..." 1>&2
START=$(date -Is)
SOURCES=$(timeout "$TIMEOUT" tcpdump -i "$INTERFACE" -nnql dst port 514 or dst port 6514 2>/dev/null| awk '{ ip = gensub(/([0-9]+.[0-9]+.[0-9]+.[0-9]+)(.*)/,"\\1","g",$3); if(!d[ip]) { print ip; d[ip]=1; fflush(stdout) } }')
END=$(date -Is)

# Output
echo -e "\n" >> "$LOGFILE"
echo -n > "$OUTFILE"
echo -e "### $(hostname | awk -F\. '{print $1}') - DUMP FINISHED!\n### Duration: $TIMEOUT ($START to $END)" | tee -a "$LOGFILE" "$OUTFILE"
echo "$SOURCES" | tee -a "$LOGFILE" "$OUTFILE"
