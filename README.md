# SCIP log forwarder playbooks

This repository contains an Ansible role for setting up a simple Logstash installation that forwards Syslog input to a RabbitMQ cluster.

Check out the [TekDokWiki article](https://tekdokwiki.int.sonofon.dk/display/SO/THC+Log+Forwarders) for more information.

## What does it do?

We have a couple of Ansible roles:
- `common`
- `logstash`
- `sftp`
- `journalbeat`

#### The `common` role
1. Ensures Nokia's and Telenor's SSH keys are present on the `root` user.
2. Sets up Yum to use the Squid proxy
3. Creates filesystem on data volumes and mounts them

#### The `logstash` role 
This is the main role.
1. Installs headless JVM and Logstash
2. Sets up `iptables` redirect.
   Required since making a Java application listen on ports less than `1024` is a pain in the butt. We listen on `5140` and iptables redirects all traffic from `514` to this.
3. Sets up some extra system settings like increased file descriptor and `vm.max_map_count` 
4. Copies main Logstash configuration
5. Cleans and installs all pipelines
6. Optionally sets up TLS-enabled syslog input
7. Starts Logstash

#### The `sftp` role
Some components don't support other input methods, so they ship their logs through SFTP.
We use SSHD's built-in SFTP server.
1. Sets up users and directories for data upload
2. Authorizes SSH keys from RSP
3. Installs systemd service (Bash script) to watch this directory and unpack tarballs into files Logstash will read

#### The `journalbeat` role
Journalbeat is used to ship all logforwarders systemd journals to CLM.
This way we can check all their logs centrally.
1. Installs and configures Journalbeat 


## Usage

### Verify access

Since THC hosts are behind a few firewalls, you must ensure that you can connect to them through the `ivabjmp09.iux.sonofon.dk` jump host (request access).
Then you can use the OSS SSH key to log in on all forwarders.

The inventory file in this repository constains the SSH options to do so:
```
...
[all:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no -o ProxyCommand="ssh -W %h:%p -q ivabjmp09.iux.sonofon.dk"'
ansible_user=root
ansible_ssh_private_key_file=~/.ssh/id_sciplog
```
The inventory is only used for executing locally. Be sure to change these settings if your SSH setup is different.

### Exectue

Simply run the main `init.yml` playbook with Ansible. Meant to be run from AWX.
If running locally, you must have the Ansible Vault password to decrypt credentials and certificate keys. @MAALS knows this password.
```
$ ansible-playbook -i inventories/production --ask-vault-pass init.yml
```
